#!/usr/bin/env python
# Autenticación SMTP

import smtplib

mail_server = 'smtp.example.com'
mail_server_port = 25

from_addr = 'some@adress.com'
to_addr = 'some@example.com'
from_header = 'From: %s\r\n' % from_addr
to_header = 'To: %s\r\n\r\n' % to_addr
subject_header = 'Subject: Testing SMTP Authentication'

body = 'This mail tests SMTP Authentication'

email_message = '%s\n%s\n%s\n\n%s' % (from_header, to_header, subject_header, body)

s = smtplib.SMTP(mail_server, mail_server_port)
s.starttls()
s.login("usuario", "password")
s.sendmail(from_addr, to_addr, email_message)
s.quit()