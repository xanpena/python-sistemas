#!/usr/bin/env python
# PDF "Hello World"
#apt-get install python-reportlab

from reportlab.pdfgen import canvas

def hello():
    c = canvas.Canvas("helloworld.pdf")
    c.drawString(100,100,"Hello World")
    c.showPage()
    c.save()
hello()