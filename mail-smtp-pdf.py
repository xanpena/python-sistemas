#!/usr/bin/env python
# Enviar un mensaje de correo electrónico con anexo PDF

import email
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import encoders
import smtplib
import mimetypes

from_addr = 'some@adress.com'
to_addr = 'some@example.com'
subject_header = 'Subject: sending PDF Attachemt'
attachment = 'disk_usage.pdf'
body = 'This message sends a PDF attachment'

m = MIMEMultipart()
m["To"] = to_addr
m["From"] = from_addr
m["Subject"] = subject_header

ctype, encoding = mimetypes.guess_type(attachment)
print(ctype, encoding)
maintype, subtype = ctype.split('/', 1)

m.attach(MIMEText(body))
fp = open(attachment, 'rb')
msg = MIMEBase(maintype, subtype)
msg.set_payload(fp.read())
fp.close()
encoders.encode_base64(msg)
msg.add_header("Content-Disposition", "attachment", filename=attachment)
m.attach(msg)

s = smtplib.SMTP("localhost")
s.set_debuglevel(1)
s.sendmail(from_addr, to_addr, m.as_string())
s.quit()